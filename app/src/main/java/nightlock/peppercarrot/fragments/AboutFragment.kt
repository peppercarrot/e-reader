/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.fragments

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.ViewSwitcher
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import io.github.mthli.sugartask.SugarTask
import nightlock.peppercarrot.R
import nightlock.peppercarrot.databinding.FragmentAboutBinding
import nightlock.peppercarrot.utils.getEasterEggs

/**
 * Created by nightlock on 18/03/18.
 */
class AboutFragment : Fragment() {

    private var _binding: FragmentAboutBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        _binding = FragmentAboutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setCards()

        binding.aboutBackgroundSwitcher.apply {
            inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
            outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
        }

        background = context?.let { GlideSwitcher(it, binding.aboutBackgroundSwitcher) }

        updateBackground(clickCount)
        updatePictureList()
        updateBackgroundPicture()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setCards() {
        binding.layoutCardAboutApp?.apply {
            appVersion.text = getVersionCode()
            viewSourceContainer.setOnClickListener { openUrl(SOURCE_LINK) }
            viewPrivacyPolicyContainer.setOnClickListener {
                openUrl(resources.getString(R.string.privacy_policy_link))
            }

            versionContainer.setOnClickListener { easterEgg() }
            versionContainer.setOnLongClickListener {
                clickCount = -1
                updateBackground(clickCount)
                getNewPicture()
                false
            }
        }
    }

    private fun updateBackgroundPicture() {
        val preference = PreferenceManager.getDefaultSharedPreferences(requireContext())

        if (preference.contains(EASTER_EGG_PHOTO)) {
            val easterImageLink = preference.getString(EASTER_EGG_PHOTO, "")!!
            if (easterImageLink.isNotEmpty()) {
                background?.init(easterImageLink)

                preference.edit().putString(EASTER_EGG_PHOTO, "").apply()
            }
        }
    }

    private var clickCount: Int = 0
    private var easterEggLinkList: List<String> = ArrayList()
    private var background: GlideSwitcher? = null

    private fun easterEgg() {
        clickCount++
        updateBackground(clickCount)
    }

    private fun getNewPicture() {
        if (easterEggLinkList.isEmpty()) {
            binding.aboutBackground1.setImageResource(R.drawable.vertical_cover_book_three)
            binding.aboutBackground2.setImageResource(R.drawable.vertical_cover_book_three)
        } else {
            background?.setImage(easterEggLinkList.random())
        }
    }

    private fun updatePictureList() {
        if (!isAdded) return
        SugarTask.with(this).assign {
            getEasterEggs()
        }.finish { easterEggs ->
            if (easterEggs != null) {
                easterEggLinkList = easterEggs as List<String>
            }
        }.execute()
    }

    private fun updateBackground(clickCount: Int) {
        val alpha = if (clickCount * 2 <= TRANSPARENCY_LIMIT) clickCount.toFloat() * 2 / 100f
        else TRANSPARENCY_LIMIT.toFloat() / 100f

        binding.aboutBackgroundSwitcher.alpha = alpha
        binding.layoutCardAboutApp?.aboutCard?.alpha = 1 - alpha * 1.2f
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val preference = PreferenceManager.getDefaultSharedPreferences(context)

        if (preference.contains(BACKGROUND_TRANSPARENCY)) clickCount =
                preference.getInt(BACKGROUND_TRANSPARENCY, 0
                )
    }

    override fun onDetach() {
        super.onDetach()
        val preference = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val easterEggUrl = background?.imageUrl
        preference.edit().let {
            it.putInt(BACKGROUND_TRANSPARENCY, clickCount)
            if (easterEggUrl != null) {
                it.putString(EASTER_EGG_PHOTO, easterEggUrl)
            } else {
                it.putString(EASTER_EGG_PHOTO, "")
            }
            it.apply()
        }
    }

    private fun getVersionCode(): String {
        try {
            return requireContext().packageManager.getPackageInfo(requireContext().packageName, 0
            ).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return requireContext().getString(R.string.version_unknown)
    }

    private fun openUrl(url: String) {
        val i = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(url)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        startActivity(i)
    }

    private fun openEmail(email: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:$email")
            putExtra(Intent.EXTRA_EMAIL, email)
            putExtra(Intent.EXTRA_SUBJECT, "Reader for Pepper&Carrot")
        }
        startActivity(Intent.createChooser(intent, "E-Mail"))
    }

    private inner class GlideSwitcher(private val context: Context,
            private val viewSwitcher: ViewSwitcher) {

        var imageUrl: String? = null
            private set

        fun setImage(imageUrl: String?) {
            if (imageUrl != null) show(imageUrl) else clear()
            this.imageUrl = imageUrl
        }

        fun init(url: String) {
            val current = viewSwitcher.currentView as ImageView
            Glide
                    .with(context)
                    .load(url)
                    .apply(RequestOptions().override(current.width, current.height).centerCrop())
                    .error(R.drawable.vertical_cover_book_three)
                    .into(current)
            this.imageUrl = url
        }

        private fun show(url: String) {
            val current = viewSwitcher.currentView as ImageView
            val next = viewSwitcher.nextView as ImageView
            Glide
                    .with(context)
                    .load(url)
                    .apply(RequestOptions().override(current.width, current.height).centerCrop())
                    .listener(listener)
                    .error(R.drawable.vertical_cover_book_three)
                    .into(next)
        }

        private fun clear() {
            val current = viewSwitcher.currentView as ImageView
            Glide.with(context).clear(current)
        }

        private val listener = object : RequestListener<Drawable> {

            override fun onResourceReady(resource: Drawable, model: Any,
                    target: Target<Drawable>?, dataSource: DataSource,
                    isFirstResource: Boolean): Boolean {
                viewSwitcher.showNext()
                return false
            }

            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>,
                    isFirstResource: Boolean): Boolean {
                viewSwitcher.showNext()
                return false
            }
        }
    }

    companion object {
        const val SOURCE_LINK = "https://framagit.org/peppercarrot/e-reader"
        const val EMAIL_ADDRESS_NIGHTLOCK = "imsesaok@tuta.io"
        const val BACKGROUND_TRANSPARENCY = "about_background_transparency"
        const val EASTER_EGG_PHOTO = "about_background_online_link"
        const val TRANSPARENCY_LIMIT = 35
    }
}
