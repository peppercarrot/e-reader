/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.fragments

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomViewTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import io.github.mthli.sugartask.SugarTask
import nightlock.peppercarrot.R
import nightlock.peppercarrot.activities.ComicViewerActivity
import nightlock.peppercarrot.databinding.FragmentComicViewerBinding
import nightlock.peppercarrot.utils.getSafePrefString
import nightlock.peppercarrot.utils.jsonToList
import nightlock.peppercarrot.utils.pokeAt

/**
 * Created by nightlock on 5/7/17.
 */

class ComicViewerFragment : Fragment() {
    private val anim by lazy { AnimationUtils.loadAnimation(context, R.anim.blink) }
    private lateinit var imgLink: String
    private var _binding: FragmentComicViewerBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        if (arguments != null && requireArguments().containsKey(ARG_LINK)) {
            imgLink = requireArguments().getString(ARG_LINK)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        _binding = FragmentComicViewerBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.comicImage.apply {
            setOnClickListener {
                val activity = activity as ComicViewerActivity
                activity.toggle()
            }
            setOnImageEventListener(object :
                    SubsamplingScaleImageView.DefaultOnImageEventListener() {
                override fun onReady() {
                    super.onReady()
                    setBackgroundColor(Color.WHITE)
                }
            })
        }

        Glide
                .with(this)
                .asBitmap()
                .load("$imgLink.jpg")
                .into(object : CustomViewTarget<SubsamplingScaleImageView, Bitmap>(binding.comicImage) {
                    override fun onResourceReady(resource: Bitmap,
                            transition: Transition<in Bitmap>?) {
                        binding.comicImage.setImage(ImageSource.bitmap(resource))
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        checkGif("$imgLink.gif")
                    }

                    override fun onResourceCleared(placeholder: Drawable?) {
                    }
                })

        binding.loadingImage.startAnimation(anim)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun checkGif(link: String) {
        //TODO: TEMPORARY CODE CREATED FOR GIF COMPATIBILITY. Edit when the API includes the full file URL.
        SugarTask.with(this).assign {
            val preference = PreferenceManager.getDefaultSharedPreferences(requireContext())
            val availableGifs = jsonToList(getSafePrefString(preference, GIF_AVAILABLE, "[]"))!!
            var available = availableGifs.contains(link)

            if (!available && pokeAt(link)) {
                preference.edit().apply {
                    availableGifs.add(link)
                    putString(GIF_AVAILABLE, nightlock.peppercarrot.utils.listToJson(availableGifs)
                    )
                    apply()
                }
                available = true
            }

            return@assign available
        }.finish { available ->
            if (!(available as Boolean)) {
                clearAnimation()

                binding.loadingImage.setImageDrawable(
                        ContextCompat.getDrawable(requireContext(), R.drawable.error_placeholder
                        )
                )
                return@finish
            }
            Glide
                    .with(this)
                    .asGif()
                    .load(link)
                    .apply(RequestOptions()
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.DATA)
                            .error(R.drawable.error_placeholder)
                    )
                    .listener(object : RequestListener<GifDrawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?,
                                target: Target<GifDrawable>, isFirstResource: Boolean): Boolean =
                                clearAnimation()

                        override fun onResourceReady(resource: GifDrawable, model: Any,
                                target: Target<GifDrawable>?, dataSource: DataSource,
                                isFirstResource: Boolean): Boolean {
                            binding.loadingImage.clearAnimation()
                            binding.loadingImage.alpha = 0f
                            return false
                        }
                    })
                    .into(binding.gifLoader)
        }.broken {
            Log.e("crystal_ball", it.message.toString())
            clearAnimation()
            val errorDrawable =
                    ContextCompat.getDrawable(requireContext(), R.drawable.error_placeholder
                    )
            binding.loadingImage.setImageDrawable(errorDrawable)
        }.execute()
    }

    private fun clearAnimation(): Boolean {
        binding.loadingImage.clearAnimation()
        binding.loadingImage.alpha = 1f
        return false
    }

    companion object {
        private const val ARG_LINK = "link"
        private const val GIF_AVAILABLE = "tested_gif"

        fun newInstance(link: String): ComicViewerFragment {
            val fragment = ComicViewerFragment()
            val args = Bundle().apply {
                putString(ARG_LINK, link)
            }
            fragment.arguments = args
            return fragment
        }
    }
}
