/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.fragments

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import io.github.mthli.sugartask.SugarTask
import nightlock.peppercarrot.R
import nightlock.peppercarrot.adapters.ArchiveAdapter
import nightlock.peppercarrot.databinding.FragmentArchiveBinding
import nightlock.peppercarrot.utils.ArchiveDataManager
import nightlock.peppercarrot.utils.Constants
import nightlock.peppercarrot.utils.Episode
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.VarColumnGridLayoutManager
import java.io.IOException
import javax.net.ssl.SSLHandshakeException

/**
 * Fragment holding a list of Episode covers of a given Language
 * Created by Jihoon Kim on 4/30/17.
 */

class ArchiveFragment : Fragment() {
    private var archiveAdapter: ArchiveAdapter? = null
    private var _binding: FragmentArchiveBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        archiveAdapter =
                arguments?.getParcelable<Language>(ARG_LANGUAGE)?.let { ArchiveAdapter(it) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            bundle: Bundle?): View {
        _binding = FragmentArchiveBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, bundle: Bundle?) {
        super.onViewCreated(view, bundle)

        val recyclerState = bundle?.getParcelable<Parcelable>(RECYCLER_VIEW_POS)

        context?.let { ArchiveDataManager(it) }?.let { init(it, recyclerState) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val recyclerViewState = binding.archiveRecycler.layoutManager?.onSaveInstanceState()
        outState.putParcelable(RECYCLER_VIEW_POS, recyclerViewState)
    }

    private fun init(db: ArchiveDataManager, recyclerState: Parcelable?) {
        binding.archiveRecycler.apply {
            layoutManager = VarColumnGridLayoutManager(context, Constants.PC_COVER_WIDTH_PX).apply {
                onRestoreInstanceState(recyclerState)
            }
            adapter = archiveAdapter
        }

        binding.archiveRefresher.setOnRefreshListener {
            binding.archiveRefresher.isRefreshing = false
            initArchive()
        }

        if (db.length() < 1) initArchive()
        else loaded(db.getAll())
        /*
        WorkManager.getInstance().getWorkInfosByTagLiveData(ComicUpdateWorker.WORK_TAG)
                .observe(this, Observer { info ->
                    if (info != null && info.isNotEmpty() && info[info.lastIndex].state.isFinished) {
                        val myResult = info[info.lastIndex].outputData.getIntArray("added")
                        val loadedList = ArrayList<Episode>()
                        if (myResult != null) {
                            for (id in myResult){
                                db.get(ArchiveDataManager.COLUMN_NAME_INDEX, id.toString())?.let {
                                    loadedList.add(it)
                                }
                            }
                        }
                        loaded(loadedList)
                    }
                })
                */
    }

    private fun loaded(episodes: List<*>) {
        episodes.forEach { archiveAdapter?.addAndNotify(it as Episode) }

        val isInvert =
                PreferenceManager
                        .getDefaultSharedPreferences(requireContext())
                        .getBoolean(Constants.INVERT_ORDER_PREF, false)

        val invert = if (isInvert) -1 else 1

        archiveAdapter?.sortList { e1, e2 ->
            val ans = when {
                e1.index == e2.index -> 0
                e1.index > e2.index  -> 1
                else                 -> -1
            }

            ans * invert
        }
    }

    private fun initArchive() {
        binding.archiveProgress.showLoading()

        SugarTask.with(this).assign {
            ArchiveDataManager.updateArchive(requireContext())
        }.finish { list ->
            binding.archiveProgress.showContent()
            loaded(list as List<*>)
        }.broken { e ->
            if (e is IOException || e is SSLHandshakeException) onNetworkError(e)
            else throw e
        }.execute()
    }

    private fun onNetworkError(e: Exception) {
        binding.archiveProgress.showError(R.drawable.error_placeholder, "Network Error",
                "Connect to network and try again", "Retry"
        ) {
            initArchive()
        }
        Log.e("crystal_ball", "Error on initArchive()")
        e.printStackTrace()
    }

    companion object {
        private const val ARG_LANGUAGE = "language"
        private const val RECYCLER_VIEW_POS = "recyclerview_state"

        fun newInstance(language: Language): ArchiveFragment {
            val fragment = ArchiveFragment()
            val args = Bundle()
            args.putParcelable(ARG_LANGUAGE, language)
            fragment.arguments = args
            return fragment
        }
    }
}
