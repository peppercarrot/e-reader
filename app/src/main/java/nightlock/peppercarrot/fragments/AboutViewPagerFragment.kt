/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.fragments

import android.os.Bundle
import android.view.View
import nightlock.peppercarrot.adapters.AboutViewPagerAdapter

/**
 * Fragment for About screen
 * Created by Jihoon Kim on 4/30/17.
 */

class AboutViewPagerFragment : ViewPagerFragment() {

    override fun onViewCreated(view: View, bundle: Bundle?) {
        super.onViewCreated(view, bundle)
        binding.viewpager.adapter = AboutViewPagerAdapter(childFragmentManager, requireContext())
    }
}
