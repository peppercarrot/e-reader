/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.vlonjatg.progressactivity.ProgressRelativeLayout
import io.github.mthli.sugartask.SugarTask
import nightlock.peppercarrot.R
import nightlock.peppercarrot.databinding.ActivitySplashBinding
import nightlock.peppercarrot.fragments.LanguageSelectionFragment
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.LanguageDataManager
import java.io.IOException
import javax.net.ssl.SSLHandshakeException

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding

    @SuppressLint("ApplySharedPref")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val preference = PreferenceManager.getDefaultSharedPreferences(this)

        if (preference.getBoolean(MainActivity.SPLASH_SEEN, false)) {
            binding.confirmActionButton.visibility = View.GONE
            loaded()
            return
        } else {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }

        binding.confirmActionButton.setOnClickListener {
            if (!preference.contains(Language.SELECTED_LANGUAGES)) {
                return@setOnClickListener
            }
            LanguageDataManager(applicationContext).close()
            preference
                    .edit()
                    .putBoolean(MainActivity.SPLASH_SEEN, true)
                    .commit() //To make sure the value is changed *before* starting MainActivity we use a blocking commit() function.

            val mainIntent = Intent(this, MainActivity::class.java)
            startActivity(mainIntent)
            finish()
        }
        initLanguage(binding.splashActivity)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initLanguage(progress: ProgressRelativeLayout) {
        progress.showLoading()

        SugarTask.with(this).assign {
            LanguageDataManager.updateLanguage(applicationContext)
        }.finish {
            progress.showContent()
            loaded()
        }.broken { e ->
            if (e is IOException || e is SSLHandshakeException) onError(progress, e)
            else throw e
        }.execute()
    }

    private fun onError(progress: ProgressRelativeLayout, e: Exception) {
        progress.showError(R.drawable.error_placeholder, "Network Error",
                "Connect to network and try again", "Retry"
        ) {
            initLanguage(progress)
        }
        e.printStackTrace()
    }

    private fun loaded() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.splash_frame, LanguageSelectionFragment())
                .commit()
    }
}
