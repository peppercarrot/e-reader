/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.activities

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import nightlock.peppercarrot.R
import nightlock.peppercarrot.TabListener
import nightlock.peppercarrot.databinding.ActivityMainBinding
import nightlock.peppercarrot.fragments.AboutViewPagerFragment
import nightlock.peppercarrot.fragments.ArchiveViewPagerFragment
import nightlock.peppercarrot.fragments.PreferenceFragment
import nightlock.peppercarrot.utils.refreshWork

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener,
        TabListener {
    private val fragmentList = SparseArray<Fragment>()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        launchWelcomeActivity()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun launchWelcomeActivity() {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        if (!pref.getBoolean(SPLASH_SEEN, false)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                (getSystemService(NOTIFICATION_SERVICE
                ) as NotificationManager).createNotificationChannel(
                        NotificationChannel(UPDATE_NOTIF_ID,
                                getString(R.string.name_channel_updates),
                                NotificationManager.IMPORTANCE_DEFAULT
                        ).apply {
                            description = getString(R.string.description_channel_updates)
                        })
            }

            val welcomeIntent = Intent(this, SplashActivity::class.java)
            startActivity(welcomeIntent)
            finish()
        }
    }

    private fun init() {
        //Put Fragments into the list and load ArchiveFragment on FrameLayout
        fragmentList.append(R.id.nav_archive, getFragment(R.id.nav_archive))
        fragmentList.append(R.id.nav_settings, getFragment(R.id.nav_settings))
        fragmentList.append(R.id.nav_about, getFragment(R.id.nav_about))

        swapFragment(getFragment(R.id.nav_archive))
        initNavigationBar()
    }

    private fun initNavigationBar() = binding.mainBottomNav.apply {
        setOnNavigationItemSelectedListener(this@MainActivity)
        selectedItemId = R.id.nav_archive
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        swapFragment(getFragment(item.itemId))
        return true
    }

    private fun getFragment(id: Int): Fragment = fragmentList[id] ?: when (id) {
        R.id.nav_archive -> ArchiveViewPagerFragment()
        R.id.nav_settings -> PreferenceFragment()
        R.id.nav_about -> AboutViewPagerFragment()
        else -> {
            Log.wtf("crystal_ball", "Transition to Unknown Fragment Requested!")
            throw RuntimeException("Transition to Unknown Fragment Requested!")
        }
    }

    private fun swapFragment(fragment: Fragment) = supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_frame, fragment)
            .commit()

    override fun onAttached(): TabLayout = binding.mainTab.apply {
        visibility = View.VISIBLE
    }

    override fun onDetached(): TabLayout = binding.mainTab.apply {
        visibility = View.GONE
        removeAllTabs()
    }

    override fun onStop() {
        super.onStop()
        if (fragmentList.size() > 0) refreshWork(this)
    }

    companion object {
        const val SPLASH_SEEN = "splash_seen"
        const val UPDATE_NOTIF_ID = "update_notif"
    }
}
