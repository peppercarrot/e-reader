/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.adapters

import android.content.Context
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.preference.PreferenceManager
import nightlock.peppercarrot.fragments.ArchiveFragment
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.LanguageDataManager
import nightlock.peppercarrot.utils.getSafePrefString
import nightlock.peppercarrot.utils.jsonToList

/**
 * Adapter for Displaying ArchiveFragment
 * Created by nightlock on 06/02/18.
 */
class ArchiveViewPagerAdapter(fm: FragmentManager, context: Context) :
        FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val languages: List<Language>

    init {
        val manager = PreferenceManager.getDefaultSharedPreferences(context)
        val selectedLangsJson = getSafePrefString(manager, Language.SELECTED_LANGUAGES, "")
        val langCodes = jsonToList(selectedLangsJson)?.sorted() ?: ArrayList()
        val db = LanguageDataManager(context)

        languages = ArrayList<Language>().apply {
            for (langCode in langCodes) db.get(langCode)?.let { add(it) }
        }
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {
        try {
            super.restoreState(state, loader)
        } catch (e: IllegalStateException) {
        } catch (e: NullPointerException) {
        }
    }

    override fun getItem(position: Int): Fragment = ArchiveFragment.newInstance(languages[position])

    override fun getCount(): Int = languages.size

    override fun getPageTitle(position: Int): CharSequence = languages[position].localName

}
