/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mikepenz.aboutlibraries.LibsBuilder
import nightlock.peppercarrot.R
import nightlock.peppercarrot.fragments.AboutFragment

class AboutViewPagerAdapter(fm: FragmentManager, val context: Context) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0    -> AboutFragment()
            1    -> LibsBuilder().supportFragment()
            else -> throw IllegalArgumentException(
                "Unknown index \"$position\" given in AboutViewPagerAdapter"
            )
        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0    -> context.getString(R.string.about_menu)
            1    -> context.getString(R.string.about_license)
            else -> throw IllegalArgumentException(
                "Unknown index \"$position\" given in AboutViewPagerAdapter"
            )
        }
    }

}
