/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.utils

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

@Suppress("IMPLICIT_CAST_TO_ANY")
abstract class BaseCacheDataManager<T>(context: Context, dbName: String,
        private val tableName: String, version: Int, private val idColumn: String,
        columns: Map<String, String>) : SQLiteOpenHelper(context, dbName, null, version) {

    private val columnNames = columns.keys.toTypedArray()

    private val sqlCreateEntries: String
    private val sqlDeleteEntries = "DROP TABLE IF EXISTS $tableName"
    private val sqlSelectAll = "SELECT * FROM $tableName"

    init {
        val str = StringBuilder("CREATE TABLE $tableName (")
        for (column in columns) {
            val name = column.key
            val type = column.value
            str.append("$name $type, ")
        }
        //columns.forEach { name, type ->  }
        str.deleteCharAt(str.lastIndexOf(","))
        sqlCreateEntries = str.append(")").toString()
    }

    override fun onCreate(db: SQLiteDatabase) {
        Log.i("crystal_ball", sqlCreateEntries)
        db.execSQL(sqlCreateEntries)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(sqlDeleteEntries)
        onCreate(db)
    }

    fun put(data: T): Boolean = writableDatabase.use { db ->
        //Check for duplicates and updates.
        var entryExists = false
        val cursor =
                db.query(tableName, columnNames, "$idColumn = ?", arrayOf(getIdValue(data)), null,
                        null, null, null
                )
        cursor.use {
            entryExists = cursor.moveToFirst()
            if (entryExists && getFromCursor(cursor) == data) return false //Identical ROW exists
        }

        val values = toContentValues(data)

        if (entryExists) db.update(tableName, values, "$idColumn = ?", arrayOf(getIdValue(data)))
        else db.insert(tableName, null, values)
        return true
    }

    fun length(): Int = readableDatabase.use { db ->
        db.rawQuery(sqlSelectAll, null).use { cursor ->
            return cursor.count
        }
    }

    fun get(id: String): T? = get(idColumn, id)

    fun get(name: String, value: String): T? = readableDatabase.use { db ->
        db
                .query(tableName, columnNames, "$name = ?", arrayOf(value), null, null, null, null)
                .use { cursor ->
                    return if (cursor.moveToFirst()) getFromCursor(cursor) else null
                }
    }

    fun getAll(): List<T> = readableDatabase.use { db ->
        val list = ArrayList<T>()
        db.rawQuery(sqlSelectAll, null).use { cursor ->
            if (cursor.moveToFirst()) do list += getFromCursor(cursor) while (cursor.moveToNext())
        }
        return list
    }

    abstract fun getFromCursor(cursor: Cursor): T
    abstract fun toContentValues(data: T): ContentValues
    abstract fun getIdValue(data: T): String
}
