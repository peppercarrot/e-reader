/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import nightlock.peppercarrot.activities.MainActivity
import nightlock.peppercarrot.utils.*
import javax.net.ssl.SSLHandshakeException

class ComicUpdateWorker(val context: Context, params: WorkerParameters) : Worker(context, params) {

    override fun doWork(): Result {
        val notification =
                NotificationCompat
                        .Builder(context, MainActivity.UPDATE_NOTIF_ID)
                        .setSmallIcon(R.drawable.ic_launcher_web)
                        .setContentTitle(context.getString(R.string.new_updates))
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentIntent(PendingIntent.getActivity(context, NOTIF_ID,
                                Intent(context, MainActivity::class.java).addFlags(
                                        Intent.FLAG_ACTIVITY_NEW_TASK
                                ), PendingIntent.FLAG_ONE_SHOT
                        )
                        )

        context.let {
            try {
                val updated = ArchiveDataManager.updateArchive(it)
                LanguageDataManager.updateLanguage(it)

                if (updated.isEmpty()) return Result.success()

                val manager = PreferenceManager.getDefaultSharedPreferences(it)
                val selectedLangsJson = getSafePrefString(manager, Language.SELECTED_LANGUAGES, "[]")
                val selectedLanguages = jsonToList(selectedLangsJson)
                val languageDb = LanguageDataManager(it)

                val addedList = IntArray(updated.size)
                val notifContainer = NotificationCompat.InboxStyle()
                for ((i, episode) in updated.withIndex()) {
                    episode.supported_languages
                            .asSequence()
                            .filter { language -> selectedLanguages?.contains(language) ?: false }
                            .forEach { language ->
                                val langName = languageDb.get(language)!!.localName
                                val line =
                                        it.getString(R.string.update_entry, episode.index + 1,
                                                langName
                                        )
                                notifContainer.addLine(line)
                            }
                    addedList[i] = episode.index
                }

                if (addedList.isEmpty()) return Result.success()

                notification.setStyle(notifContainer)
                NotificationManagerCompat.from(it).notify(NOTIF_ID, notification.build())

                val resultData = Data.Builder().putIntArray("added", addedList).build()

                return Result.success(resultData)
            } catch (e: SSLHandshakeException) {
                Log.e("crystal_ball", "SSLHandshakeException; Spoofed Connection?")
            }
            return Result.retry()
        }
    }

    companion object {
        const val NOTIF_ID = 42
        const val WORK_TAG = "comic_updater"
    }

}
