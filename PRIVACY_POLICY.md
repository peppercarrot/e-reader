Pepper&Carrot e-reader ("the APP") *does not* store or transmit personal information.

The APP connects to the [Sources](https://www.peppercarrot.com/0_sources/) page of the [Official Pepper&Carrot Website](https://www.peppercarrot.com/), *only* to serve the core purpose of the APP.
No personal data are transmitted in the process, and the APP does not handle cookies.

The APP will identify itself with an anonymous [user agent](https://en.wikipedia.org/wiki/User_agent) that reveals to the server the APP's version. Information about your device or user is not included.
Refer to the [Privacy Policy of Pepper&Carrot](https://www.peppercarrot.com/en/tos/index.html) for information on what is collected by the website.

This Privacy Policy is applicable from version 1.3 and onwards.
See changes and revisions: https://framagit.org/peppercarrot/e-reader/-/commits/master/PRIVACY_POLICY.md

This document was last updated on September 25, 2024.